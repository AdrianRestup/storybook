﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ClickableObject : MonoBehaviour {
	[SerializeField] Animator anim;
	[SerializeField] TextObject objectName;
	[SerializeField] AudioSource sfx;

	[SerializeField] bool disableAfterAnim = false;

	// Use this for initialization
	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => {
			PlayAnim();
		});
	}

	// Update is called once per frame
	void Update () {
		if (sfx) {
			if (PlayerPrefs.GetInt ("SoundSetting", 1) == 0)
				sfx.volume = 0f;
			else
				sfx.volume = 0.3f;
		}
	}

	private void PlayAnim() {
		if(anim)
			anim.SetTrigger ("PlayAnim");
		if (sfx)
			sfx.Play ();
		if(objectName)
			objectName.Show (2f);

		if (disableAfterAnim)
			GetComponent<Button> ().interactable = false;
	}

	public void ResetState() {
		anim.SetTrigger ("Reset");
		GetComponent<Button> ().interactable = true;
	}
}
