﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PageManager : MonoBehaviour {
	[Header("Screen")]
	[SerializeField] GameObject splashScreen;
	[SerializeField] GameObject mainMenuScreen;
	[SerializeField] GameObject settingScreen;
	[SerializeField] GameObject storyPagesScreen;
	[SerializeField] GameObject pauseScreen;
	[SerializeField] Image fadeScreen;

	[Header("Page")]
	[SerializeField] RectTransform pageContainer;
	[SerializeField] int pageCount;
	[SerializeField] StoryPage page1;
	[SerializeField] StoryPage page2;
	[SerializeField] StoryPage page3;
	[SerializeField] StoryPage page4;
	[SerializeField] StoryPage page5;
	[SerializeField] StoryPage page6;
	[SerializeField] StoryPage page7;
	[SerializeField] StoryPage page8;
	[SerializeField] StoryPage page9;
	[SerializeField] StoryPage page10;
	[SerializeField] StoryPage page11;
	[SerializeField] StoryPage page12;
	[SerializeField] StoryPage page13;
	[SerializeField] StoryPage page14;
	[SerializeField] StoryPage creditScreen;

	[Header("Button")]
	[SerializeField] Button goToStoryButton;
//	[SerializeField] Button settingButton;
	[SerializeField] Button backToHomeButton;
	[SerializeField] Button prevButton;
	[SerializeField] Button nextButton;

	[Header("Story Screen Button")]
	[SerializeField] Button pauseButton;
	[SerializeField] Button resumeButton;
	[SerializeField] Button backToHomeFromStoryButton;
	[SerializeField] Button resetStoryButton;

	[Header("Popup Exit")]
	[SerializeField] GameObject popupExit;
	[SerializeField] Button yesButton;
	[SerializeField] Button noButton;

	[Header("BGM")]
	[SerializeField] AudioSource menuBGM;
	[SerializeField] AudioSource storyBGM;
	[SerializeField] AudioSource moralBGM;

	private float lerp;
	private bool isChangingScreen, isChangingPage;
	private int pageIndex;
	private GameObject currScreen, toScreen;
	// Story Page
	private bool isFirstPage;
	private float fromX, toX;

	public int SPLASH = 1;
	public int MAIN_MENU = 2;
	public int STORY_PAGES = 3;
	public int SETTING_MENU = 4;

	[Header("Delay")]
	[SerializeField] float delaySplash;

	// Use this for initialization
	void Start () {
		lerp = 0f;
		pageIndex = 1;
		isChangingScreen = false;

		// Init screen
		splashScreen.SetActive (true);
		mainMenuScreen.SetActive (false);
		settingScreen.SetActive (false);
		storyPagesScreen.SetActive (false);
		pauseScreen.SetActive (false);

		// Popup
		popupExit.SetActive(false);

		fadeScreen.color = new Color (0f, 0f, 0f, 0f);

		currScreen = splashScreen;

		// Button
		goToStoryButton.onClick.AddListener(() => ChangeScreen(STORY_PAGES));
//		settingButton.onClick.AddListener (() => ChangeScreen(SETTING_MENU));
		backToHomeButton.onClick.AddListener (() => ChangeScreen(MAIN_MENU));
		// Story Page Button
		pauseButton.onClick.AddListener(() => PauseState());
		resumeButton.onClick.AddListener (() => ResumeState ());
		backToHomeFromStoryButton.onClick.AddListener (() => BackToHome ());
		// Popup Exit
		yesButton.onClick.AddListener(() => ExitFromApp());
		noButton.onClick.AddListener (() => ClosePopupExit());
		// TODO settings
		prevButton.onClick.AddListener(() => { ChangePage(pageIndex - 1); });
		nextButton.onClick.AddListener(() => { ChangePage(pageIndex + 1); });

		ChangeScreen (MAIN_MENU);
	}
	
	// Update is called once per frame
	void Update () {
		// Check exit
		bool backPressed = Input.GetKeyDown(KeyCode.Escape);
		if (backPressed) {
			if (popupExit.activeInHierarchy)
				popupExit.SetActive (false);
			else
				popupExit.SetActive (true);
		}

		// Check sound state
		if (PlayerPrefs.GetInt ("SoundSetting", 1) == 0) {
			menuBGM.volume = 0f;
			storyBGM.volume = 0f;
			moralBGM.volume = 0f;
		} else {
			menuBGM.volume = 0.3f;
			storyBGM.volume = 0.3f;
			moralBGM.volume = 0.3f;
		}

		if (delaySplash > 0f) {
			delaySplash -= Time.deltaTime;
		} else {
			if (isChangingScreen) {
				if (lerp >= 1f) {
					currScreen.SetActive (false);
					toScreen.SetActive (true);
					currScreen = toScreen;
					lerp = 0f;

					if (isFirstPage) {
						page1.Show ();
						isFirstPage = false;
					}

					if (toScreen == mainMenuScreen) {
						if (pauseScreen.activeInHierarchy)
							pauseScreen.SetActive (false);
						
						if (!menuBGM.isPlaying) {
							menuBGM.Play ();
							storyBGM.Stop ();
							moralBGM.Stop ();
						}

						GetCurrentPage ().ResetPage ();
						GetCurrentPage ().voiceOver.Stop ();
						pauseScreen.SetActive (false);
						pageIndex = 1;
						pageContainer.anchoredPosition = new Vector2 (0f, 0f);
					}

					if (toScreen == storyPagesScreen) {
						if (!storyBGM.isPlaying) {
							menuBGM.Stop ();
							storyBGM.Play ();
							moralBGM.Stop ();
						}
					}

					isChangingScreen = false;
				} else {
					lerp += Time.deltaTime * 0.8f;

					float a = Mathf.Lerp (0f, 1f, lerp);
					fadeScreen.color = new Color (0f, 0f, 0f, a);
				}
			} else {
				if (lerp >= 1f) {

					fadeScreen.gameObject.SetActive (false);
				} else {
					lerp += Time.deltaTime * 1.5f;

					float a = Mathf.Lerp (1f, 0f, lerp);
					fadeScreen.color = new Color (0f, 0f, 0f, a);
				}
			}

			// Check page
			if (pageIndex == 1) {
				prevButton.gameObject.SetActive (false);
				nextButton.gameObject.SetActive (true);
			} else if (pageIndex == pageCount) {
				prevButton.gameObject.SetActive (true);
				nextButton.gameObject.SetActive (false);
			} else {
				prevButton.gameObject.SetActive (true);
				nextButton.gameObject.SetActive (true);
			}

			if (isChangingPage) {
				if (lerp >= 1f) {
					isChangingPage = false;
				} else {
					lerp += Time.deltaTime * 1.5f;

					float posX = Mathfx.Sinerp (fromX, toX, lerp);
					pageContainer.anchoredPosition = new Vector2 (posX, 0f);
				}
			}
		}
	}

	// Change Screen
	public void ChangeScreen(int toIndex) {
		switch (toIndex) {
		case 1:
			toScreen = splashScreen;
			break;

		case 2:
			toScreen = mainMenuScreen;
//			menuBGM.Play ();
//			moralBGM.Stop ();
			break;

		case 3:
			toScreen = storyPagesScreen;
			isFirstPage = true;
//			menuBGM.Stop ();
//			storyBGM.Play ();
			break;

		case 4:
			toScreen = settingScreen;
			break;
		}
		fadeScreen.gameObject.SetActive (true);
		lerp = 0f;
		isChangingScreen = true;
	}

	public void ChangePage(int toIndex, bool isReset = false) {
		if (!isChangingPage) {
			switch (toIndex) {
			case 1:
				page1.Show ();
				break;

			case 2:
				page2.Show ();
				break;

			case 3:
				page3.Show ();
				break;

			case 4:
				page4.Show ();
				break;

			case 5:
				page5.Show ();
				break;

			case 6:
				page6.Show ();
				break;

			case 7:
				page7.Show ();
				break;

			case 8:
				page8.Show ();
				break;

			case 9:
				page9.Show ();
				break;
				// TODO next page

			case 10:
				page10.Show ();
				break;

			case 11:
				page11.Show ();
				break;

			case 12:
				page12.Show ();
				break;

			case 13:
				page13.Show ();
				break;

			case 14:
				page14.Show ();
				break;

			case 15: // TODO change last page
//				creditScreen.Show(isStoryPage: false);
				creditScreen.Show();
				break;
			}

			fromX = -(pageIndex - 1) * 1134f;

			GetCurrentPage ().ResetPage ();

			if (isReset) {
				pageIndex = 1;
				toX = 0f;
			} else {
				if (toIndex > pageIndex) {
					pageIndex += 1;
					toX = fromX - 1134f;
				} else {
					pageIndex -= 1;
					toX = fromX + 1134f;
				}
			}
			lerp = 0f;
			isChangingPage = true;
		}
	}

	private StoryPage GetCurrentPage() {
		switch (pageIndex) {
		case 1:
			return page1;

		case 2:
			return page2;

		case 3:
			return page3;

		case 4:
			return page4;

		case 5:
			return page5;

		case 6:
			return page6;

		case 7:
			return page7;

		case 8:
			return page8;

		case 9:
			return page9;
			// TODO next page

		case 10:
			return page10;

		case 11:
			return page11;

		case 12:
			return page12;

		case 13:
			return page13;

		case 14:
			return page14;

		case 15: // TODO change to last page
			return creditScreen;
		}
		return null;
	}

	public void PlayMoralBGM() {
		storyBGM.Stop ();
		moralBGM.Play ();
	}

	public void PauseState() {
		GetCurrentPage ().voiceOver.Pause ();
		pauseScreen.SetActive (true);
	}

	public void ResumeState() {
		GetCurrentPage ().voiceOver.Play ();
		pauseScreen.SetActive (false);
	}

	// From story
	public void BackToHome() {
		storyBGM.Stop ();
		ChangeScreen(2);
	}

	public void RestartStory() {
		GetCurrentPage ().voiceOver.Stop ();
		GetCurrentPage ().ResetPage ();
		pageIndex = 1;
		pageContainer.anchoredPosition = new Vector2 (0f, 0f);
		pauseScreen.SetActive (false);
		page1.Show ();
	}

	// Popup exit
	public void ClosePopupExit() {
		popupExit.SetActive (false);
	}

	public void ExitFromApp() {
		Application.Quit ();
	}
}
