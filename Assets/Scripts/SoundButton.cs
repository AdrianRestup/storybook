﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundButton : MonoBehaviour {
	[SerializeField] Sprite stateOn;
	[SerializeField] Sprite stateOff;

	private Image image;

	private bool isOn;

	// Use this for initialization
	void Start () {
		isOn = PlayerPrefs.GetInt ("SoundSetting", 1) == 1;
		image = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isOn) {
			image.sprite = stateOn;
		} else {
			image.sprite = stateOff;
		}
	}

	public void ChangeState() {
		if (isOn) {
			// set to off
			isOn = false;
			PlayerPrefs.SetInt ("SoundSetting", 0);
		} else {
			isOn = true;
			PlayerPrefs.SetInt ("SoundSetting", 1);
		}
		PlayerPrefs.Save ();
	}
}
