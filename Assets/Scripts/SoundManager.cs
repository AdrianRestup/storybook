﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	[SerializeField] AudioSource menuBGM;
	[SerializeField] AudioSource storyBGM;
	[SerializeField] AudioSource moralBGM;

	private AudioSource currBGM;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("SoundSetting", 1) == 0) {
			currBGM.volume = 0f;
		} else {
			currBGM.volume = 1f;
		}
	}

	public void PlayMenuBGM() {
		currBGM.Stop ();
		menuBGM.Play ();
		currBGM = menuBGM;
	}

	public void PlayStoryBGM() {
		currBGM.Stop ();
		storyBGM.Play ();
		currBGM = storyBGM;
	}

	public void PlayMoralBGM() {
		currBGM.Stop ();
		moralBGM.Play ();
		currBGM = moralBGM;
	}
}
