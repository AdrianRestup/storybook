﻿using UnityEngine;
using System.Collections;

public class GameButton : MonoBehaviour {
	private RectTransform rectTransform;

	// Use this for initialization
	void Start () {
		rectTransform = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onClick() {
		rectTransform.localScale = new Vector3 (0.9f, 0.9f, 1f);
	}

	public void onRelease() {
		rectTransform.localScale = new Vector3 (1f, 1f, 1f);
	}
}
