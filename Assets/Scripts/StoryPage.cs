﻿using UnityEngine;
using System.Collections;

public class StoryPage : MonoBehaviour {
	public TextObject narration;
	public GameObject page;

	[SerializeField] ClickableObject[] objs;
	[SerializeField] GameObject[] basicObjs;

	[Header("Sound")]
	public AudioSource voiceOver;

	private PageManager pageManager;
	private bool isActive;

	// Use this for initialization
	void Start () {
		pageManager = GameObject.Find ("PageManager").GetComponent<PageManager>();
	}

	void Update() {
		if (PlayerPrefs.GetInt ("SoundSetting", 1) == 0)
			voiceOver.volume = 0f;
		else
			voiceOver.volume = 0.8f;
	}
	
	public void Show(bool isStoryPage = true) {
		if (isStoryPage) {
			isActive = true;
			narration.Reset ();
			voiceOver.Play ();
			StartCoroutine (DelayPage (1f));
		} else {
			// Play Credit Anim
			pageManager.PlayMoralBGM();
			StartCoroutine (DelayPage (1f));
		}
	}

	private IEnumerator DelayPage(float delay) {
		yield return new WaitForSeconds (delay);
		if (isActive) {
			page.SetActive (true);
			narration.Show (1.5f);

		}
	}

	public void ResetPage() {
		isActive = false;
		voiceOver.Stop ();

		foreach (ClickableObject obj in objs) {
			obj.ResetState ();
		}

		foreach (GameObject obj in basicObjs) {
			obj.GetComponent<Animator> ().SetTrigger ("Reset");
		}
	}
}
