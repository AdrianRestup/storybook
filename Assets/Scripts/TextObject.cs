﻿using UnityEngine;
using System.Collections;

public class TextObject : MonoBehaviour {
	[SerializeField] bool isNarration;
	private RectTransform rectTransform;
	private float lerp;
	private bool isAnimating;

	private float speed;

	void Awake() {
		rectTransform = GetComponent<RectTransform> ();
		lerp = 0f;

		rectTransform.localScale = new Vector3 (0f, 0f, 0f);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isAnimating) {
			if (lerp >= 1f) {
				if (!isNarration) {
					StartCoroutine (DelayShrink (1f));
				}
				isAnimating = false;
			} else {
				lerp += Time.deltaTime * speed;

				float scale = Mathfx.Berp (0f, 1f, lerp);
				rectTransform.localScale = new Vector3 (scale, scale, 1f);
			}
		}
	}

	public void Show(float speed = 1f) {
		if (!isAnimating) {
			lerp = 0f;
			this.speed = speed;
			isAnimating = true;
		}
	}

	private IEnumerator DelayShrink(float delay) {
		yield return new WaitForSeconds (delay);

		if(!isAnimating)
			rectTransform.localScale = new Vector3 (0f, 0f, 0f);
	}

	public void Reset() {
		rectTransform.localScale = new Vector3 (0f, 0f, 0f);
	}
}
